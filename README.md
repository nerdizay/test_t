# Тестовое задание 
Тут описание

## Запуск
Скопировать переименовать .env.example в .env и
при необходимости что-нибудь поменять (необходимости нет)

Запустить виртуальное окружение
```
poetry shell
```

Установить зависимости
```
poetry install
```

Провести миграции (sqlite)
```
python manage.py migrate
```

Запустить сервер
```
python manage.py runserver
```

Запустить тесты
```
python manage.py test
```

По роуту api/convert_work_shedule можно протестировать вручную