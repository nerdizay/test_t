from django.test import TestCase, Client
from django.test import RequestFactory
from app.views import WorkSheduleView
import json
from icecream import ic


class BaseTest(TestCase):
    
    def setUp(self):
        self.api_url = "/api/convert_work_shedule"
        self.factory = RequestFactory()
        self.handle_request = WorkSheduleView.as_view()
        self.client = Client()
        
    def test_base_example(self):
        request = self.factory.post(self.api_url, data={
            "monday" : [], 
            "tuesday" : [ 
                { 
                "type" : "open", 
                "value" : 36000 
                }, 
                { 
                "type" : "close", 
                "value" : 64800 
                } 
            ], 
            "wednesday" : [], 
            "thursday" : [ 
                { 
                "type" : "open", 
                "value" : 37800 
                }, 
                { 
                "type" : "close", 
                "value" : 64800 
                } 
            ], 
            "friday" : [ 
                { 
                "type" : "open", 
                "value" : 36000 
                } 
            ], 
            "saturday" : [ 
                { 
                "type" : "close", 
                "value" : 3600 
                }, 
                { 
                "type" : "open", 
                "value" : 36000 
                } 
            ], 
            "sunday" : [ 
                { 
                "type" : "close", 
                "value" : 3600 
                }, 
                {
                "type" : "open", 
                "value" : 43200 
                }, 
                { 
                "type" : "close", 
                "value" : 75600 
                } 
            ] 
        }, content_type="application/json")
        
        response = self.handle_request(request)
        
        self.assertEqual(response.data, {
            "Monday": "Closed",
            "Tuesday": "10 AM - 6 PM",
            "Wednesday": "Closed",
            "Thursday": "10:30 AM - 6 PM",
            "Friday": "10 AM - 1 AM",
            "Saturday": "10 AM - 1 AM",
            "Sunday": "12 PM - 9 PM"
        })
        
    def test_close_more_than_one_day(self):
        request = self.factory.post(self.api_url, data={
            "monday" : [{"type" : "open", "value" : 36000}],
            "tuesday" : [{"type" : "close", "value" : 36100}],
            "wednesday" : [],
            "thursday" : [],
            "friday" : [],
            "saturday" : [],
            "sunday" : []
        } , content_type="application/json")
        
        response = self.handle_request(request)
        
        self.assertEqual(response.status_code, 400)
        
    def test_not_closed(self):
        request = self.factory.post(self.api_url, data={
            "monday" : [{"type" : "open", "value" : 36100}],
            "tuesday" : [],
            "wednesday" : [],
            "thursday" : [],
            "friday" : [],
            "saturday" : [],
            "sunday" : []
        } , content_type="application/json")
        
        response = self.handle_request(request)
        
        self.assertEqual(response.status_code, 400)
        
    def test_not_opened(self):
        request = self.factory.post(self.api_url, data={
            "monday" : [{"type" : "close", "value" : 36100}],
            "tuesday" : [],
            "wednesday" : [],
            "thursday" : [],
            "friday" : [],
            "saturday" : [],
            "sunday" : []
        } , content_type="application/json")
        
        response = self.handle_request(request)
        
        self.assertEqual(response.status_code, 400)
        
    def test_monday_sunday(self):
        request = self.factory.post(self.api_url, data={
            "monday" : [{"type" : "close", "value" : 12000}],
            "tuesday" : [],
            "wednesday" : [],
            "thursday" : [],
            "friday" : [],
            "saturday" : [],
            "sunday" : [{"type" : "open", "value" : 36000}]
        } , content_type="application/json")
        
        response = self.handle_request(request)
        
        self.assertEqual(response.data, {
            "Monday": "Closed",
            "Tuesday": "Closed",
            "Wednesday": "Closed",
            "Thursday": "Closed",
            "Friday": "Closed",
            "Saturday": "Closed",
            "Sunday": "10 AM - 3:20 AM"
        })
        
    
