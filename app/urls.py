from django.urls import path
from .views import WorkSheduleView


urlpatterns = [
    path('convert_work_shedule', WorkSheduleView.as_view()),
]