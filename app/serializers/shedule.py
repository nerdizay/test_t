from rest_framework import serializers
from core.settings import TIMESTAMP_MIN, TIMESTAMP_MAX


TYPE_TIME_ITEM = (
    ('open', 'open'),
    ('close', 'close'),
)

DAYS_OF_WEEK_LIST = [
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
    'sunday',
]


class TimeItemSerializer(serializers.Serializer):
    type = serializers.ChoiceField(choices=TYPE_TIME_ITEM)
    value = serializers.IntegerField(min_value=TIMESTAMP_MIN, max_value=TIMESTAMP_MAX)
    
class SheduleSerializer(serializers.Serializer):
    monday = serializers.ListField(child=TimeItemSerializer(), allow_empty=True)
    tuesday = serializers.ListField(child=TimeItemSerializer(), allow_empty=True)
    wednesday = serializers.ListField(child=TimeItemSerializer(), allow_empty=True)
    thursday = serializers.ListField(child=TimeItemSerializer(), allow_empty=True)
    friday = serializers.ListField(child=TimeItemSerializer(), allow_empty=True)
    saturday = serializers.ListField(child=TimeItemSerializer(), allow_empty=True)
    sunday = serializers.ListField(child=TimeItemSerializer(), allow_empty=True)
    
class HumanReadableSheduleSerializer(serializers.Serializer):
    Monday = serializers.CharField()
    Tuesday = serializers.CharField()
    Wednesday = serializers.CharField()
    Thursday = serializers.CharField()
    Friday = serializers.CharField()
    Saturday = serializers.CharField()
    Sunday = serializers.CharField()
        

    
