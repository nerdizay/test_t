from rest_framework.views import APIView
from rest_framework.response import Response
from app.work_sheduler_converter import WorkSheduleConverter
from app.serializers.shedule import SheduleSerializer, HumanReadableSheduleSerializer
from icecream import ic
import dotsi


class WorkSheduleView(APIView):
    
    def post(self, request):
        shedule_serializer = SheduleSerializer(data=request.data)
        
        if not shedule_serializer.is_valid():
            return Response({"error": shedule_serializer.errors})
        
        work_sheduler_converter = WorkSheduleConverter()
        result = work_sheduler_converter.convert_to_human_readable(dotsi.Dict(shedule_serializer.data))
        human_readable_shedule_serializer = HumanReadableSheduleSerializer(data=result)
        
        if not human_readable_shedule_serializer.is_valid():
            return Response({"error": human_readable_shedule_serializer.errors})
        
        return Response(result)
