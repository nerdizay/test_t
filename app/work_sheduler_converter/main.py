from enum import Enum
from datetime import datetime
from rest_framework.exceptions import ValidationError
from app.serializers.shedule import (TimeItemSerializer,
                                     SheduleSerializer,
                                     HumanReadableSheduleSerializer)
from core.settings import TIMESTAMP_MAX
import re
from icecream import ic


class DayOfWeekManager:
    LIST_DAYS_OF_WEEK = [
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
        "sunday"
    ]
    class EnumDaysOfWeek(Enum):
        MONDAY = "monday"
        TUESDAY = "tuesday"
        WEDNESDAY = "wednesday"
        THURSDAY = "thursday"
        FRIDAY = "friday"
        SATURDAY = "saturday"
        SUNDAY = "sunday"
        
    @staticmethod
    def get_previous_day(day: EnumDaysOfWeek):
        return DayOfWeekManager.LIST_DAYS_OF_WEEK[DayOfWeekManager.LIST_DAYS_OF_WEEK.index(day)-1]
    

class WorkSheduleConverter:
    CLOSED = "Closed"

    def convert_to_human_readable(self, work_shedule: SheduleSerializer) -> HumanReadableSheduleSerializer:
        self.work_shedule = work_shedule
        work_shedule = self.prepare()
        res = {}
        for day in work_shedule.copy():
            day_info = self.convert_all_times_to_human_readable(
                self.get_open_times(work_shedule[day]),
                self.get_close_times(work_shedule[day])
            )
            res[day.title()] = day_info
        return res
    
    def set_current_day_name(self, day: DayOfWeekManager.EnumDaysOfWeek) -> None:
        self.current_day = day
        
    def get_current_day_name(self) -> DayOfWeekManager.EnumDaysOfWeek:
        return self.current_day
        
    def get_previous_day_name(self) -> DayOfWeekManager.EnumDaysOfWeek:
        return DayOfWeekManager.get_previous_day(self.current_day)
    
    def get_current_day(self) -> list[TimeItemSerializer]:
        return self.work_shedule[self.get_current_day_name()]
    
    def get_previous_day(self) -> list[TimeItemSerializer]:
        return self.work_shedule[self.get_previous_day_name()]
    
    def get_open_times(self, day: list[TimeItemSerializer]) -> list[TimeItemSerializer]:
        res = list(filter(lambda x: x.get('type') == 'open', day))
        return res
    
    def get_close_times(self, day: list[TimeItemSerializer]) -> list[TimeItemSerializer]:
        res = list(filter(lambda x: x.get('type') == 'close', day))
        return res
    
    def get_open_times_current_day(self) -> list[TimeItemSerializer]:
        return self.get_open_times(self.get_current_day())
    
    def get_close_times_current_day(self) -> list[TimeItemSerializer]:
        return self.get_close_times(self.get_current_day())
        
    def get_min_open_times_current_day(self) -> TimeItemSerializer:
        try:
            return min(self.get_open_times_current_day(), key=lambda shedule_item: shedule_item.value)
        except ValueError:
            return None
    
    def get_min_close_times_current_day(self) -> TimeItemSerializer:
        try:
            return min(self.get_close_times_current_day(), key=lambda shedule_item: shedule_item.value)
        except ValueError:
            return None
        
    def get_min_close_times_previous_day(self) -> TimeItemSerializer:
        try:
            return min(self.get_close_times(self.get_previous_day()), key=lambda shedule_item: shedule_item.value)
        except ValueError:
            return None
        
    def get_min_open_times_previous_day(self) -> TimeItemSerializer:
        try:
            return min(self.get_open_times(self.get_previous_day()), key=lambda shedule_item: shedule_item.value)
        except ValueError:
            return None
    
    def get_max_open_times_current_day(self) -> TimeItemSerializer:
        try:
            return max(self.get_open_times_current_day(), key=lambda shedule_item: shedule_item.value)
        except ValueError:
            return None
    
    def get_max_close_times_current_day(self) -> TimeItemSerializer:
        try:
            return max(self.get_close_times_current_day(), key=lambda shedule_item: shedule_item.value)
        except ValueError:
            return None
        
    def get_count_open_times_current_day(self) -> int:
        return len(self.get_open_times_current_day())
        
    def get_count_close_times_current_day(self) -> int:
        return len(self.get_close_times_current_day())
    
    def get_count_open_times_previoys_day(self) -> int:
        return len(self.get_open_times(self.get_previous_day()))
    
    def get_count_close_times_previous_day(self) -> int:
        return len(self.get_close_times(self.get_previous_day()))
    
    def prepare_timestamp_by_day(self) -> None:
        for day in self.work_shedule:
            for shedule_item in self.work_shedule[day]:
                shedule_item.value = (
shedule_item.value + DayOfWeekManager.LIST_DAYS_OF_WEEK.index(day) * TIMESTAMP_MAX)
    
    def prepare(self) -> SheduleSerializer:
        """
        Функция предназначена для подготовки данных, 
        для удобного оперирования с ними в дальнейшем
        Она переводит закрытие смены на предыдущий день, 
        если закрытие происходит на следующий день.
        """
        self.prepare_timestamp_by_day()
        for day in self.work_shedule:
            self.set_current_day_name(day)
            min_close_times_current_day = self.get_min_close_times_current_day()
            min_open_times_current_day = self.get_min_open_times_current_day()
            min_open_times_previous_day = self.get_min_open_times_previous_day()
            
            if not min_close_times_current_day:
                continue
            
            if (min_close_times_current_day and
                not min_open_times_current_day and
                not min_open_times_previous_day
            ):
                raise ValidationError(f"Нельзя иметь закрытие смены без открытия \
и не иметь открытия смены в предыдущем дне. см {day} и {DayOfWeekManager.get_previous_day(day)}")
                
            if not min_open_times_previous_day:
                if self.get_count_open_times_current_day() != self.get_count_close_times_current_day():
                    raise ValidationError(f"Так как в {DayOfWeekManager.get_previous_day(day)} нет открытия смены, \
то в следующий день - {day} - кол-во открытий смен должно быть равно кол-ву закрытий")
                else:
                    continue
                   
            if (min_close_times_current_day.value - TIMESTAMP_MAX)%TIMESTAMP_MAX - min_open_times_previous_day.value%TIMESTAMP_MAX > 0:
                raise ValidationError(f"Нельзя, чтобы закрытие смены было позже чем открытие на сутки. \
см. {day} и {DayOfWeekManager.get_previous_day(day)} {min_open_times_previous_day} {min_close_times_current_day}")
                  
            if self.get_count_open_times_previoys_day() == self.get_count_close_times_previous_day():
                continue
            
            if self.get_count_open_times_previoys_day() - self.get_count_close_times_previous_day() > 1:
                raise ValidationError(f"Невозможно чтобы было несколько незакрытых смен \
в одном дне. см. {day} и {DayOfWeekManager.get_previous_day(day)}")
                
            self.work_shedule[DayOfWeekManager.get_previous_day(day)].append(min_close_times_current_day)
            self.work_shedule[day].remove(min_close_times_current_day)
            
        for day in self.work_shedule:
            self.set_current_day_name(day)
            if self.get_count_open_times_current_day() != self.get_count_close_times_current_day():
                raise ValidationError(f"Кажется смена открыта но не закрыта. \
см. {day} and {DayOfWeekManager.get_previous_day(day)}")
            
        return self.work_shedule
        
    def convert_one_time_to_human_readable(self, timestamp: int) -> str:
        dt_object = datetime.utcfromtimestamp(timestamp)
        res = dt_object.strftime("%I:%M %p").replace(":00", "")
        res = re.sub(r'^0', '', res)
        return res
    
    def convert_all_times_to_human_readable(
        self,
        open_times: list[TimeItemSerializer],
        close_times: list[TimeItemSerializer]
    ) -> str:
        res = []
        for i in range(len(open_times)):
            open_time = self.convert_one_time_to_human_readable(open_times[i].value)
            close_time = self.convert_one_time_to_human_readable(close_times[i].value)
            res.append(f"{open_time} - {close_time}")
        res = ", ".join(res)
        if not res:
            return self.CLOSED
        return res


if __name__ == "__main__":
    
    shedule = { 
        "monday" : [], 
        "tuesday" : [ 
            { 
            "type" : "open", 
            "value" : 36000 
            }, 
            { 
            "type" : "close", 
            "value" : 64800 
            } 
        ], 
        "wednesday" : [], 
        "thursday" : [ 
            { 
            "type" : "open", 
            "value" : 37800 
            }, 
            { 
            "type" : "close", 
            "value" : 64800 
            } 
        ], 
        "friday" : [ 
            { 
            "type" : "open", 
            "value" : 36000 
            } 
        ], 
        "saturday" : [ 
            { 
            "type" : "close", 
            "value" : 3600 
            }, 
            { 
            "type" : "open", 
            "value" : 36000 
            } 
        ], 
        "sunday" : [ 
            { 
            "type" : "close", 
            "value" : 3600 
            }, 
            {
            "type" : "open", 
            "value" : 43200 
            }, 
            { 
            "type" : "close", 
            "value" : 75600 
            } 
        ] 
    } 

    work_sheduler_cnverter = WorkSheduleConverter()
    res = work_sheduler_cnverter.convert_to_human_readable(shedule)
    print(res)
