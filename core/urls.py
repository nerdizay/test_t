from django.contrib import admin
from django.urls import path, include
from rest_framework.schemas import get_schema_view


api_urls = (
    *include('app.urls'),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        "openapi",
        get_schema_view(title="Your Project", description="API for all things …", version="1.0.0"),
        name="openapi-schema",
    ),
    path('api/', api_urls),
]
